function wait(sec) {
	var count = 0
	var isStop = false
	while (!isStop) {
		count++
		if (count === sec) {
			isStop = true;
		}
	}
}

var TRTracker = (function TRTracker() {
	function TRTracker() {
		var _tableRows = document.querySelectorAll('.k-grid-content > table tr');
		this.tableRows = Array.apply(null, _tableRows);

		var _tableRowsEditBtn = document.querySelectorAll('.k-grid-content-locked > table tr');
		this.tableRowsEditBtn = Array.apply(null, _tableRowsEditBtn);
	}

	TRTracker.prototype.searchProjectIDIndex = function (value) {
		var index = this.tableRows.findIndex(function (element, index, array) {
			return element.textContent.search(new RegExp(value, 'i')) >= 0;
		})

		return index;
	}

	TRTracker.prototype.clickEditBtn = function (index) {
		this.tableRowsEditBtn[index].querySelector('a').click();
	}

	TRTracker.prototype.setStatusDropDownBox = function (status) {

		var _status = status || 'active';

		var enumStatus = {
			Active: 1,
			Completed: 3,
			on_hold: 5
		}
		document.querySelector('#StatusDropdownId_listbox > li:nth-child(' + enumStatus[_status] + ')').click();

		if (status === 'Completed') {
			//set the close date 30 days out
			TRTracker.setProjectCloseDate();
		}
	}

	TRTracker.prototype.setNeedNewGoLiveDate = function (newGoLiveDate, reason) {
		//set the need new go live date to true
		document.querySelector('#IsNeedNewGoLiveDate').click()
		document.querySelector('#RequestNewGoLiveDate').value = newGoLiveDate
                               
		//set reason
		TRTracker.setSlippedReason(reason);
	}

	TRTracker.setSlippedReason = function (reason) {

		var _reason = reason || 'client_related_issues';
                               
		/**
		*
		 * @enum {number}
		*/

		var enumStatus = {
			client_related_issues: 1,
			osi_issues: 2,
			other: 3
		}
		document.querySelector('#SlippedReasonDropdownId_listbox > li:nth-child(' + enumStatus[_reason] + ')').click();

		TRTracker.setSlippedComment();
	}

	TRTracker.setSlippedComment = function () {
		var d = new Date();
		var todayDate = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
		var comment = todayDate + ",FA,Client Related Issues"
		document.querySelector('#SlippedConversionComment').value = comment
	}

	TRTracker.setProjectCloseDate = function () {

		var future = new Date();
		future.setDate(future.getDate() + 30);

		var futureDate = (future.getMonth() + 1) + '/' + future.getDate() + '/' + future.getFullYear();

		document.querySelector('#DateForProjectClose').value = futureDate
	}

	TRTracker.prototype.update = function (arg) {
		//test boolean
		if (arg && arg.test) {       
			//clicks the cancel button
			document.querySelector('.k-grid-cancel').click();
			return;
		}

		document.querySelector('.k-grid-update').click();


	}

	TRTracker.prototype.filterLst = function (goLiveDate) {
                               
		//set status to Active
		document.querySelector('[data-title="Status"] > a.k-grid-filter').click()

		document.querySelector('body > div.k-animation-container > form > div:nth-child(1) > span:nth-child(3) > span > span.k-input').click()

		document.querySelector('body > div.k-animation-container > form > div.k-animation-container > div > div:nth-child(3) > ul > li:nth-child(1)').click();
		//click the filter button
		document.querySelector('body > div.k-animation-container > form > div:nth-child(1) > div:nth-child(7) > button.k-button.k-primary').click();
                               
		//set project manager to Falos Akers
		document.querySelector('[data-title="Project Manager"] > a.k-grid-filter').click()

		document.querySelector('div.k-animation-container > div > div:nth-child(3) > ul > li:nth-child(21)').click();
		//click the filter button
		document.querySelector('[style*="display: block"] button.k-button.k-primary').click();
                               
		//init the filter grid for Go-Live Date
		document.querySelector('[data-title="Go-Live Date"] > a.k-grid-filter').click()
		//set go-live date for first item
		document.querySelector('body > div.k-animation-container > form > div:nth-child(1) > span:nth-child(3) > span > span.k-input').click()
		document.querySelector('div.k-animation-container > div > div:nth-child(2) > ul > li:nth-child(3)').click();
		document.querySelector('form > div:nth-child(1) > span:nth-child(3) > span > input').value = '1/1/2015';
		//set go-live date for second item
		document.querySelector('body > div.k-animation-container > form > div:nth-child(1) > span:nth-child(5) > span > span.k-input').click()
		document.querySelector('div.k-animation-container > div > div:nth-child(2) > ul > li:nth-child(5)').click();
		document.querySelector('form > div:nth-child(1) > span:nth-child(6) > span > input').value = goLiveDate;
		//Click filter button
		document.querySelector('<button type="submit" class="k-button k-primary">Filter</button>').click();
	}

	return TRTracker;
})()

var UploadFile = (function UploadFile() {
	function UploadFile() {

	}

	UploadFile.prototype.addUploadBtn = function () {
		var clearFilterBtn = document.querySelector('#formGrid > div.k-header.k-grid-toolbar.k-grid-top > div > a:nth-child(2)');
		var uploadBtn = document.createElement('input');
		//uploadBtn.appendChild(document.createTextNode('Upload'));
		uploadBtn.setAttribute('type', 'file');
		uploadBtn.setAttribute('id', 'file');
		uploadBtn.className = 'k-button k-button-icontext';
		uploadBtn.onchange = UploadFile.handleFile;
		clearFilterBtn.parentNode.insertBefore(uploadBtn, clearFilterBtn.nextSibling);
	}

	UploadFile.handleFile = function (evt) {
		var file = evt.target.files[0]; // FileList object
 
		var reader = new FileReader();
 
		// Closure to capture the file information.
		reader.onload = (function (theFile) {
			return function (e) {
				/**
				* list of projects to update
				* split the file based on return key
				* @type {array}
				*/
				var projects = e.target.result.split('\n');
                                                               
				//init app
				var $TRTracker = new TRTracker();

				projects.forEach(function (value, idx, array) {
					/**
					* a project split by commas
					* i.e. PR028944,Active,9/22/2015
					* @type {array}
					*/
					var project = value.split(',');
                                                                               
					/**
					* enum of project array data
					* @enum {number}
					*/
					var project_enum = {
						clarityNumber: 0,
						status: 1,
						date: 2
					}
                                                                               
					//search project based on clarity id
					var index = $TRTracker.searchProjectIDIndex(project[project_enum.clarityNumber]);

					if (index !== -1) {
 
						//click the edit button for the clarity project
						var isLoop = true
						if (document.querySelector('.k-grid-update') === null) {
							$TRTracker.clickEditBtn(index);
							isLoop = false;
						}


						if (project[project_enum.status].trim() === 'Completed') {
							//change the status if needed
							$TRTracker.setStatusDropDownBox('Completed');
							$TRTracker.update();
						} else {
							//change the status if needed
							$TRTracker.setStatusDropDownBox('Active');
							//change the go-live, set slip reason and comment
							//slip reason default to client_related_issues
							//comment default to
							$TRTracker.setNeedNewGoLiveDate(project[project_enum.date]);
							$TRTracker.update();
						}
 
					}

				});



				console.log(e.target.result.split('\n'));
			};
		})(file);
 
		// Read in the text file as a text.
		reader.readAsText(file);
	}
	return UploadFile;
})()

var load = new UploadFile();
load.addUploadBtn();